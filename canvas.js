const canvas = document.getElementById("webcanvas");
const ctx = canvas.getContext("2d");
var color = document.getElementById("color");
color.addEventListener("input", ()=>{
    ctx.strokeStyle = color.value;
})
ctx.lineJoin = "round";
ctx.lineCap = "round";
var select = document.getElementById("select");
select.addEventListener("input", ()=>{
    ctx.lineWidth = select.value;
})

var x_end = 0;
var y_end = 0;
var drawing = false;
var eraser = false;
var pencil = true;
var rect = false;
var f_rect = false;
var tri = false;
var f_tri = false;
var cir = false;
var f_cir = false;
var ImgArray = new Array();
var step = 0;

function draw(e){
    if(!drawing) return;
    ctx.beginPath();
    ctx.moveTo(x_end, y_end);
    ctx.lineTo(e.offsetX, e.offsetY);
    ctx.stroke();
    x_end = e.offsetX;
    y_end = e.offsetY;
}
function erase(e){
    if(!drawing) return;
    ctx.clearRect(x_end, y_end, ctx.lineWidth, ctx.lineWidth);
    x_end = e.offsetX;
    y_end = e.offsetY;
}
function draw_rect(e){
    if(!drawing) return;
    ctx.putImageData(ImgArray[step], 0, 0);
    ctx.beginPath();
    ctx.strokeRect(x_end,y_end,e.offsetX-x_end,e.offsetY-y_end);
    ctx.stroke();
}
function draw_fillrect(e){
    if(!drawing) return;
    ctx.putImageData(ImgArray[step], 0, 0);
    ctx.beginPath();
    ctx.fillRect(x_end,y_end,e.offsetX-x_end,e.offsetY-y_end);
    ctx.stroke();
}
function draw_cir(e){
    if(!drawing) return;
    ctx.putImageData(ImgArray[step], 0, 0);
    ctx.beginPath();
    ctx.arc(x_end, y_end, Math.abs(e.offsetX - x_end), 0, 2 * Math.PI);
    ctx.stroke();
}
function draw_fillcir(e){
    if(!drawing) return;
    ctx.putImageData(ImgArray[step], 0, 0);
    ctx.beginPath();
    ctx.arc(x_end, y_end, Math.abs(e.offsetX - x_end), 0, 2 * Math.PI);
    ctx.fill();
    ctx.stroke();
}
function draw_tri(e){
    if(!drawing) return;
    ctx.putImageData(ImgArray[step], 0, 0);
    ctx.beginPath();
    ctx.moveTo(x_end,y_end);
    ctx.lineTo(e.offsetX,e.offsetY);
    ctx.lineTo(e.offsetX,e.offsetY+(y_end-e.offsetY)*2);
    ctx.lineTo(x_end,y_end);
    ctx.stroke();
}
function draw_filltri(e){
    if(!drawing) return;
    ctx.putImageData(ImgArray[step], 0, 0);
    ctx.beginPath();
    ctx.moveTo(x_end,y_end);
    ctx.lineTo(e.offsetX,e.offsetY);
    ctx.lineTo(e.offsetX,e.offsetY+(y_end-e.offsetY)*2);
    ctx.lineTo(x_end,y_end);
    ctx.fill();
    ctx.stroke();
}
function Push_next(){
    if(step < ImgArray.length){
        ImgArray.length = step + 1;
    }
    step++;
    ImgArray.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
}
function Undo(){
    if(step > 0){
        step--;
        ctx.putImageData(ImgArray[step], 0, 0);
    }
}
function Redo(){
    if(step < ImgArray.length - 1){
        step++;
        ctx.putImageData(ImgArray[step], 0, 0);
    }
}
function PencilClick(){
    pencil = true;
    eraser = false;
    rect = false;
    cir = false;
    f_rect = false;
    f_cir = false;
    tri = false;
    f_tri = false;
}
function EraserClick(){
    pencil = false;
    eraser = true;
    rect = false;
    cir = false;
    f_rect = false;
    f_cir = false;
    tri = false;
    f_tri = false;
}
function RectClick(){
    pencil = false;
    eraser = false;
    rect = true;
    cir = false;
    f_rect = false;
    f_cir = false;
    tri = false;
    f_tri = false;
}
function fillRectClick(){
    pencil = false;
    eraser = false;
    rect = false;
    cir = false;
    f_rect = true;
    f_cir = false;
    tri = false;
    f_tri = false;
}
function CirClick(){
    pencil = false;
    eraser = false;
    rect = false;
    cir = true;
    f_rect = false;
    f_cir = false;
    tri = false;
    f_tri = false;
}
function fillCirClick(){
    pencil = false;
    eraser = false;
    rect = false;
    cir = false;
    f_rect = false;
    f_cir = true;
    tri = false;
    f_tri = false;
}
function TriClick(){
    pencil = false;
    eraser = false;
    rect = false;
    cir = false;
    f_rect = false;
    f_cir = false;
    tri = true;
    f_tri = false;
}
function fillTriClick(){
    pencil = false;
    eraser = false;
    rect = false;
    cir = false;
    f_rect = false;
    f_cir = false;
    tri = false;
    f_tri = true;
}

ImgArray.push(ctx.getImageData(0, 0, canvas.width, canvas.height));

canvas.addEventListener("mousedown", (e)=>{
    drawing = true;
    x_end = e.offsetX;
    y_end = e.offsetY;
});
canvas.addEventListener("mousemove", (e)=>{
    if(drawing){
        if(pencil) draw(e);
        else if(eraser) erase(e);
        else if(rect) draw_rect(e);
        else if(cir) draw_cir(e);
        else if(f_rect) draw_fillrect(e);
        else if(f_cir) draw_fillcir(e);
        else if(tri) draw_tri(e);
        else if(f_tri) draw_filltri(e);
    }
});
canvas.addEventListener("mousedown", ()=>{
    drawing = true;
});
canvas.addEventListener("mouseup", ()=>{
    drawing = false;
    Push_next();
});
canvas.addEventListener("mouseout", ()=>{
    drawing = false;
});

document.getElementById("clear").addEventListener("click", ()=>{
    ctx.clearRect(0, 0, canvas.width, canvas.height);
})
download_img = (e)=>{
    var imageURL = canvas.toDataURL("image/jpg");
    e.href = imageURL;
};

document.getElementById('inp').onchange = function (e){
    var img = new Image();
    img.onload = draws;
    img.onerror = failed;
    img.src = URL.createObjectURL(this.files[0]);
};
function draws() {    
    ctx.drawImage(this, 0,0);
}
function failed() {
    console.error("The provided file couldn't be loaded as an Image media");
}
