# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | N         |
| Cursor icon                                      | 10%       | N         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| upload                                           | :         | Y         |
| no_filled_shape                                  | :         | Y         |


---

### How to use 

    按下旁邊各種按鈕來做不同的事

### Function description

    圖形就是先用鼠標位置畫，再根據鼠標位置每走一步，就用存取好上一步的圖片來覆蓋，就可以
    undo redo 是在每次mouseup後用陣列存取canvas圖片，當按下按鈕就會依情況step++ or step--，並載入圖片
    橡皮擦跟垃圾桶都是用clearRect來做

### Gitlab page link

    your web page URL, which should be "https://[studentID].gitlab.io/AS_01_WebCanvas"

### Others (Optional)

    Anythinh you want to say to TAs.

<style>
table th{
    width: 100%;
}
</style>